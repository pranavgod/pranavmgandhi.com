<!DOCTYPE html>
<html>
<head>
    <title>{Pranav Gandhi} - Because you want only the best</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="/scripts.js"></script>
     <script type="text/javascript">

     var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-3231173-8']);
_gaq.push(['_trackPageview']);

(function() {
  var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>
</head>
<body>
    <div id="header">
        <h1><span class="paren">{</span>Pranav Gandhi<span class="paren">}</span></h1>
    </div>
    <div id="menu">
        <div id="links">
            <a href="#welcome">Welcome</a>
            <a href="#aboutme">About Me</a>
            <a href="#realmofracket">Realm of Racket</a>
            <a href="#cloud">Cloud</a>
            <a href="#pbrane">pBrane</a>
            <a href="http://dl.dropbox.com/u/7176171/Resume.pdf">Resume</a>
        </div>
    </div>
    <div id="container">
        
    </div>
</body>
</html>