$(document).ready(function(){

	var currentHash = window.location.hash;

	function load(page){

		$("#container").animate(
			{left: 0-$(this).width()},
			150,
			"swing",
			function(){
				$("#container").load(page+".html");
				$("#container").css("left", screen.width);
				$("#container").animate(
					{left: 0},
					150,
					"swing",
					false
				);
			}
		);

	}


	if(window.location.hash.length > 3){
		var loc = window.location.hash.substring(1);
		if(loc == "welcome"
			|| loc == "aboutme"
			|| loc == "realmofracket"
			|| loc == "cloud"
			|| loc == "pbrane")
			load(loc);
	}else
		load("welcome");

	function checkHash(){
		if(window.location.hash != currentHash){
			currentHash = window.location.hash;
			load(window.location.hash.substring(1));
		}

	}
		
	setInterval(checkHash, 10);

	// $("#links > a").live("click", function(e){
	// 	load(window.location.hash.substring(1));
	// });
	

});