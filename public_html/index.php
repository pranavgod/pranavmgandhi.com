<?php 
	require_once("core.php");
	$page = new Page();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pranav M Gandhi - <?php echo $page->getTitle(); ?></title>
<link rel="stylesheet" href="/style2.css">
<script type="text/javascript" src="/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="/scripts.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3231173-8']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="menu">Pranav M Gandhi <?php $page->generateLinks(); ?></div>
<div id="container"></div>
</body>
</html>

