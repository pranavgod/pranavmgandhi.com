$(document).ready(function(){
    $('#container, #menu').fadeOut(0).delay(200).fadeIn(300);

    if(window.location.hash.length > 2) {
	loadContent("/"+window.location.hash);
	removeActive();
	$('a[href="/'+window.location.hash+'"]').addClass('active');
    }else{
        loadContent("/#home");
    }

    
    $('#menu > a').click(function(e){
	loadContent($(this).attr('href'));
	removeActive();
	$(this).addClass('active');
    });

    function loadContent(page){
	$('#container').fadeOut(100,function(){
	    $('#container').load('/handler.php?request='+page.substr(2), function(){
		$('#container').fadeIn(100);
	    });
	});
	
    }

    function removeActive(){
	$('#menu > a').each(function(){
	    $(this).removeClass('active');
	});

    }
    
});


