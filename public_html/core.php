<?php
// The power source of the future. Welcome to the Core.

/** Represents a page */
class Page {
	
	private $db;
	private $url;
	private $page;
	
	public function __construct(){
		$this->db = sqlite_open("db.sqlite", 0777);
		$this->initURL();
		$res = sqlite_query($this->db,"Select * from pages where title = '{$this->url[0]}';");
		$num = sqlite_num_rows($res);
		if($num != 1){
			$this->page = "home";
		}else{
			$this->page = $this->url[0];
		}
		
	}
	
	public function getPage(){
	       return $this->page;
	}
	
	/** Initializes $this->url */
	private function initURL(){
		$array = explode("/", $_SERVER['REQUEST_URI']);
		array_shift($array);
		$this->url = $array;
	}

	/** Sets a new page */
	public function setPage($page){
	       $this->page = $page;
	}
	
	/** Returns the body of the page */
	public function getBody(){
		$query = sqlite_query($this->db, "SELECT body FROM pages WHERE title = '$this->page';");
		return sqlite_column($query, 'body');
	}
	
	/** Gets the title/identifier for the page */
	public function getTitle(){
		$query = sqlite_query($this->db,"SELECT link FROM pages WHERE title = '$this->page';");
		return sqlite_column($query,'link');
	}

	/** Checks if the requested page exists */
	public function pageExists($page){
	       $query = sqlite_query($this->db, "SELECT * from pages WHERE title = '$page';");
	       if(!$query) return false;
	       else return true;
	}
	
	/** Generates links to all pages in the db */
	public function generateLinks($page = null){
		$res = sqlite_query($this->db,"Select * from pages;");
		while($row = sqlite_fetch_array($res)){
			if($page == $row['title']){
				 $active = 'class="active"';
			}
			elseif($row['title'] == $this->page){
				 $active = 'class="active"';
			}
			else $active = null;
			//echo "<a href=\"/{$row['title']}\" $active>{$row['link']}</a> ";
				echo "<a href=\"/#{$row['title']}\" $active>{$row['link']}</a> ";
		}
	}



}









?>
