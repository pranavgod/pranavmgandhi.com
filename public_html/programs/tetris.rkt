;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-abbr-reader.ss" "lang")((modname tetris) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ())))
;; TETRIS
;;

(require 2htdp/image)
(require 2htdp/universe)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Data Definitions

;; A Block is a (make-block Number Number Color)
(define-struct block (x y color))

;; A Tetra is a (make-tetra Posn BSet)
;; The center point is the point around which the tetra rotates
;; when it spins.
(define-struct tetra (center blocks))

;; A Set of Blocks (BSet) is one of:
;; - empty
;; - (cons Block BSet)
;; Order does not matter.  Repetitions are NOT allowed.

;; A World is a (make-world Tetra BSet)
;; The BSet represents the pile of blocks at the bottom of the screen.
(define-struct world (tetra pile))

;;;;;;;;;; CONSTANTS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define CELL-SIZE 20)
(define CELL-SIZE/2 (/ CELL-SIZE 2))
(define GRID-WIDTH 10)
(define GRID-HEIGHT 20)

(define SCREEN-WIDTH (* CELL-SIZE GRID-WIDTH))
(define SCREEN-HEIGHT (* CELL-SIZE GRID-HEIGHT))

(define EMPTY-SCENE (empty-scene SCREEN-WIDTH SCREEN-HEIGHT))

;;;;;;;;;;;;; CONVERSIONS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; cell->pixel/x : Number -> Number
;; Converts a grid X coord to a screen X coord
(check-expect (cell->pixel/x 0) CELL-SIZE/2)
(check-expect (cell->pixel/x 3) (+ (* 3 CELL-SIZE) CELL-SIZE/2))

(define (cell->pixel/x x)
  (+ (* x CELL-SIZE) CELL-SIZE/2))

;; cell->pixel/y : Number -> Number
;; Converts a grid y coord to a screen y coord
(check-expect (cell->pixel/y 0) CELL-SIZE/2)
(check-expect (cell->pixel/y 3) (+ (* 3 CELL-SIZE) CELL-SIZE/2))

(define (cell->pixel/y y)
  (+ (* y CELL-SIZE) CELL-SIZE/2))

;; TETRAS ;;

(define O (make-tetra (make-posn 5.5 .5)
                      (list (make-block 5 0 "green")
                            (make-block 5 1 "green")
                            (make-block 6 0 "green")
                            (make-block 6 1 "green"))))

(define I (make-tetra (make-posn 8 0)
                      (list (make-block 5 0 "blue")
                            (make-block 6 0 "blue")
                            (make-block 7 0 "blue")
                            (make-block 8 0 "blue"))))

(define L (make-tetra (make-posn 7 1)
                      (list (make-block 5 1 "purple")
                            (make-block 6 1 "purple")
                            (make-block 7 0 "purple")
                            (make-block 7 1 "purple"))))

(define J (make-tetra (make-posn 5 1)
                      (list (make-block 5 1 "aqua")
                            (make-block 5 0 "aqua")
                            (make-block 6 0 "aqua")
                            (make-block 7 0 "aqua"))))

(define T (make-tetra (make-posn 6 1)
                      (list (make-block 5 1 "orange")
                            (make-block 6 1 "orange")
                            (make-block 6 0 "orange")
                            (make-block 7 1 "orange")))) 

(define Z (make-tetra (make-posn 6 1)
                      (list (make-block 5 0 "pink")
                            (make-block 6 1 "pink")
                            (make-block 6 0 "pink")
                            (make-block 7 1 "pink"))))

(define S (make-tetra (make-posn 6 0)
                      (list (make-block 5 1 "red")
                            (make-block 6 1 "red")
                            (make-block 6 0 "red")
                            (make-block 7 0 "red"))))

;;;;;;;;;;;; RENDERING ;;;;;;;;;;;;;;;;

;; img+scene : Image Posn Scene -> Scene
;; Draws the image at the given cell position in the scene
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PLACE CHECK-EXPECTS
(define (img+scene img p scn)
  (place-image img
               (cell->pixel/x (block-x p))
               (cell->pixel/y (block-y p))
               scn))
;; block+scene : Block Scene -> Scene
;; Draws the block in the given scene
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;PLACE CHECK EXPECTS
(define (block+scene block scn)
  (img+scene (overlay (square CELL-SIZE "outline" "black")
                      (square CELL-SIZE "solid" (block-color block)))
             block
             scn))
;; tetra+scene : Bset Scene -> Scene
;; Draws the set of blocks in the scene
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PLACE CHECK EXPECTS
(define (tetra+scene Bset scn)
  (cond [(empty? Bset) scn]
        [else
         (block+scene (first Bset)
                      (tetra+scene (rest Bset) scn))]))

;; world->scene : World -> Scene
;; Renders the world into an Empty Scene
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PLACE CHECK EXPECTS

(define (world->scene w)
  (tetra+scene (tetra-blocks (world-tetra w))
               (tetra+scene (world-pile w)
                            EMPTY-SCENE)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;; MOVEMENT ;;;;;;;;;;;;;;;;;;;;;;;;;
;; tetra-fall : tetra -> tetra
;; Lowers a tetra down by one cell
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; PLACE CHECK EXPECTS
#;
(check-expect (tetra-fall Z)
              (make-tetra (make-posn 1 1)
                          (list (make-block 0 1 "pink")
                                (make-block 1 2 "pink")
                                (make-block 1 1 "pink")
                                (make-block 2 2 "pink"))))


(define (tetra-fall tetra)
  (make-tetra (make-posn (posn-x (tetra-center tetra))
                         (add1 (posn-y (tetra-center tetra))))
              (Bset-fall (tetra-blocks tetra))))


;;Bset-fall : Bset -> Bset
;; Lowers all blocks in a Bset by one
#;
(check-expect (Bset-fall (tetra-blocks Z))
              (list (make-block 0 1 "pink")
                    (make-block 1 2 "pink")
                    (make-block 1 1 "pink")
                    (make-block 2 2 "pink")))
(define (Bset-fall bset)
  (cond [(empty? bset) empty]
        [else 
         (cons (block-fall (first bset))
               (Bset-fall (rest bset)))]))
;; block-fall : block -> block
;; Lowers a block by one cell
#;
(check-expect (block-fall (make-block 0 0 "green"))
              (make-block 0 1 "green"))
(check-expect (block-fall (make-block 5 6 "green"))
              (make-block 5 7 "green"))
(check-expect (block-fall (make-block 10 9 "green"))
              (make-block 10 10 "green"))

(define (block-fall block)
  (make-block (block-x block)
              (+ 1 (block-y block))
              (block-color block)))

;; tetra-rotate-ccw : tetra -> tetra
;; rotates the tetra counterclockwise
;; ========================================================================= PLACE CHECK EXPECTS
(define (tetra-rotate-ccw tetra)
  (cond [(bset-ingame? (bset-rotate-ccw (tetra-center tetra)
                                        (tetra-blocks tetra)))
         (make-tetra (tetra-center tetra)
                     (bset-rotate-ccw (tetra-center tetra)
                                      (tetra-blocks tetra)))]
        [else tetra]))



;; rotate-bset-ccw : bset -> bset
;; rotates all blocks in a bset counterclockwise
(define (bset-rotate-ccw c bset)
  (cond [(empty? bset) empty]
        [else
         (cons (block-rotate-ccw c (first bset))
               (bset-rotate-ccw c (rest bset)))]))

;; block-rotate-ccw : Posn Block -> Block
;; Rotate the block 90 degrees counterclockwise around the posn.
(define (block-rotate-ccw c b)
  (make-block (+ (posn-x c) (- (posn-y c) (block-y b)))
              (+ (posn-y c) (- (block-x b) (posn-x c)))
              (block-color b)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;; LEFT | RIGHT ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ========== CHECK IF TETRA IS IN GAME ======== ;;
;; bset-ingame? : bset -> boolean
;; returns true if all blocks in bset are within game
(check-expect (bset-ingame? (list (make-block 4 5 "pink")
                                  (make-block 1 5 "pink")))
              true)
(check-expect (bset-ingame? (list (make-block 1 5 "pink")
                                  (make-block -1 5 "pink")))
              false)
(define (bset-ingame? bset)
  (cond [(empty? bset) true]
        [(block-ingame? (first bset))
         (bset-ingame? (rest bset))]
        [else false]))

;; block-ingame? : block -> boolean
;; returns true if the block is within the game
(check-expect (block-ingame? (make-block 3 4 "pink"))
              true)
(check-expect (block-ingame? (make-block -1 4 "pink"))
              false)
(define (block-ingame? block)
  (cond [(and (>= (block-x block) 0)
              (<= (block-x block) 9))
         true]
        [else false]))

;; =============== LEFT ==================================

;; tetra-move-left : tetra -> tetra
;; moves a tetra left

(define (tetra-move-left t)
  (cond [(bset-ingame? (bset-move-left (tetra-blocks t)))
         (make-tetra (c-move-left (tetra-center t))
                     (bset-move-left (tetra-blocks t)))]
        [else t]))


;; c-move-left : posn -> posn
;; moves the center posn left
(check-expect (c-move-left (make-posn 1 2))
              (make-posn 0 2))

(define (c-move-left p)
  (make-posn (sub1 (posn-x p))
             (posn-y p)))


;; bset-move-left : bset -> bset
;; Moves all blocks in the list to the left
(check-expect (bset-move-left empty)
              empty)
(check-expect (bset-move-left (list (make-block 4 4 "pink")
                                    (make-block 3 6 "green")))
              (list (make-block 3 4 "pink")
                    (make-block 2 6 "green")))

(define (bset-move-left bset)
  (cond [(empty? bset) empty]
        [else
         (cons (block-move-left (first bset))
               (bset-move-left (rest bset)))]))



;; block-move-left : block -> block
;; Shifts a block left
(check-expect (block-move-left (make-block 4 4 "pink"))
              (make-block 3 4 "pink"))

(define (block-move-left b)
  (make-block (sub1 (block-x b))
              (block-y b)
              (block-color b)))

;; ========== RIGHT ================== ;;

;; tetra-move-right : tetra -> tetra
;; moves a tetra right

(define (tetra-move-right t)
  (cond [(bset-ingame? (bset-move-right (tetra-blocks t)))
         (make-tetra (c-move-right (tetra-center t))
                     (bset-move-right (tetra-blocks t)))]
        [else t]))


;; c-move-right : posn -> posn
;; moves the center posn right
(check-expect (c-move-right (make-posn 1 2))
              (make-posn 2 2))

(define (c-move-right p)
  (make-posn (add1 (posn-x p))
             (posn-y p)))


;; bset-move-right : bset -> bset
;; Moves all blocks in the list to the right
(check-expect (bset-move-right empty)
              empty)
(check-expect (bset-move-right (list (make-block 4 4 "pink")
                                     (make-block 3 6 "green")))
              (list (make-block 5 4 "pink")
                    (make-block 4 6 "green")))

(define (bset-move-right bset)
  (cond [(empty? bset) empty]
        [else
         (cons (block-move-right (first bset))
               (bset-move-right (rest bset)))]))

;; block-move-right : block -> block
;; Shifts a block right
(check-expect (block-move-right (make-block 4 4 "pink"))
              (make-block 5 4 "pink"))

(define (block-move-right b)
  (make-block (add1 (block-x b))
              (block-y b)
              (block-color b)))

;; ================================================================
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EVENTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; get-tetra : number -> tetra
;; Uses numbers 0-6 to retrieve a Tetra definition
(check-expect (get-tetra 6)
              S)
(define (get-tetra n)
  (cond [(= n 0) O]
        [(= n 1) I]
        [(= n 2) L]
        [(= n 3) J]
        [(= n 4) T]
        [(= n 5) Z]
        [(= n 6) S]))


;; tetra->pile : world -> world
;; adds the current tetra to the pile in the world
(define (tetra->pile w)
  (make-world (get-tetra (random 6))
              (append (world-pile w)
                      (tetra-blocks (world-tetra w)))))

;;;;;;;;;;;; HIT PILE? ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; bset-hit-pile? : bset pile -> boolean
;; compares the bset of the tetra to the bset of the pile
(define (bset-hit-pile? bset pile)
  (cond [(empty? bset) false]
        [(block<->pile (block-fall (first bset)) pile) true]
        [else (bset-hit-pile? (rest bset) pile)]))

;; block<->pile : block pile -> boolean
;; compares one block to all the blocks in a pile
(define (block<->pile b pile)
  (cond [(empty? pile) false]
        [(block=? b (first pile)) true]
        [else
         (block<->pile b (rest pile))]))


;; block=? : block1 block2 -> boolean
;; compares 2 blocks' x and y values and returns true if they are the same
;; and returns false if not
;;====================================================================== PLACE CHECK EXPECTS
(define (block=? b1 b2)
  (and (= (block-x b1) (block-x b2))
       (= (block-y b1) (block-y b2))))



;;;;;; HIT FLOOR? ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; tetra-hit-floor? : tetra -> boolean
;; checks if the tetra has hit the floor of the game
(define (tetra-hit-floor? t)
  (bset-hit-floor? (tetra-blocks t)))

;; bset-hit-floor? : bset -> boolean
;; checks if any item in the bset has hit the floor
(check-expect (bset-hit-floor? (tetra-blocks Z))
              false)
(check-expect (bset-hit-floor? (list (make-block 0 19 "pink")))
              true)

(define (bset-hit-floor? bset)
  (cond [(empty? bset) false]
        [(block-hit-floor? (first bset))
         true]
        [else
         (bset-hit-floor? (rest bset))])) 

;; block-hit-floor? : block -> boolean
;; checks if a block has landed on the floor
(check-expect (block-hit-floor? (make-block 0 19 "pink"))
              true)
(check-expect (block-hit-floor? (make-block 0 1 "pink"))
              false)
(define (block-hit-floor? b)
  (= (block-y b) 19))

;; ================================================================
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; KEYS PRESSING
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; key-press : World String -> World
;; When the key is pressd, perform expected action
(define (key-press w ke)
  (cond [(key=? ke "left")
         (make-world (tetra-move-left (world-tetra w))
                     (world-pile w))]
        [(key=? ke "right")
         (make-world (tetra-move-right (world-tetra w))
                     (world-pile w))]
        [(key=? ke "s")
         (make-world (tetra-rotate-ccw (world-tetra w))
                     (world-pile w))]
        [(key=? ke "a")
         (make-world (tetra-rotate-ccw
                      (tetra-rotate-ccw
                       (tetra-rotate-ccw (world-tetra w))))
                     (world-pile w))]
        [else (next-world w)]
        ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;; GAME OVER? ;;;;;;;;;;;;;;;;;;;;;;;;;

;; game-over? world -> boolean
;; returns true if the game is over
(define (game-over? w)
  (cond [(overflow? (world-pile w)) true]
        [else false]))

;; overflow? pile -> boolean
;; checks if the pile has reached above the screen
(define (overflow? pile)
  (cond [(empty? pile) false]
        [(<= (block-y (first pile))
             0)
         true]
        [else
         (overflow? (rest pile))]))

;; last-world : world -> scene
;; displays the last scene
(define (last-world w)
  (place-image
   (text "You lose. Good day sir!"
         12
         "red")
   100 200
   EMPTY-SCENE))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (next-world w)
  (cond [(tetra-hit-floor? (world-tetra w))
         (tetra->pile w)]
        [(bset-hit-pile? (tetra-blocks (world-tetra w)) 
                         (world-pile w))
         (tetra->pile w)]
        [else
         (make-world (tetra-fall (world-tetra w))
                     (world-pile w))]))



(big-bang (make-world (get-tetra (random 6)) empty)
          (on-tick next-world 1/2)
          (on-draw world->scene)
          (on-key key-press)
          (stop-when game-over? last-world))








